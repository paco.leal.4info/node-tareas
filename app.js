const argv = require('./config/yargs').argv;
const colors = require('colors');


const porHacer = require('./por-hacer/por-hacer');
//const argv = require('yargs').argv;

//console.log(argv);

let comando = argv._[0];

switch (comando) {
    case 'new':
        let resp = porHacer.crear(argv.description);
        console.log(resp);
        break;
    case 'list':
        let listado = porHacer.getList(argv.completed);
        for (let tarea of listado) {
            console.log('\n-----Tarea------'.green);
            console.log(tarea.description);
            console.log('Estado:', tarea.completed);
            console.log('----------------'.green);
        }
        break;
    case 'check':
        let tarea = porHacer.check(argv.description, argv.completed);
        if (tarea) {
            console.log('Revisado');
        }
        break;
    case 'remove':
        let borrado = porHacer.remove(argv.description);
        if (borrado) {
            console.log(`Borrado "${argv.description}"`);
        }
        break;
    default:
        console.log('Comando no identificado');
        break;
}