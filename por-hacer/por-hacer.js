const fs = require('fs');

let listadoPorHacer = [];

const saveDB = async() => {
    let data = JSON.stringify(listadoPorHacer);

    fs.writeFile('db/data.json', data, (err) => {
        if (err)
            throw new Error('No se ha guardado correctamente');
    });
}

const loadDB = () => {
    try {
        listadoPorHacer = require('../db/data.json');
    } catch (error) {
        listadoPorHacer = [];
    }
}

const crear = (description) => {
    loadDB();

    let porHacer = {
        description,
        completed: false,
    }

    listadoPorHacer.push(porHacer);
    saveDB();
    return `Tarea '${porHacer.description}' guardada`;
}

const check = (description, completed = true) => {
    loadDB();
    let index = listadoPorHacer.findIndex(tarea => tarea.description === description);

    if (index >= 0) {
        listadoPorHacer[index].completed = completed;
        saveDB();
        return true;
    } else {
        console.log(`No existe la tarea "${description}"`);
        return false;
    }
}

const getList = (completed = null) => {

    loadDB();

    if (completed === null) {
        return listadoPorHacer;
    }

    if (completed == 'true' || completed == true) {
        let listaFiltrada = listadoPorHacer.filter(tarea => tarea.completed);
        return listaFiltrada;
    } else {
        let listaFiltrada = listadoPorHacer.filter(tarea => !tarea.completed);
        return listaFiltrada;
    }
}

const remove = (description) => {
    loadDB();
    /*
    ESTO

    let borrado = listadoPorHacer.findIndex(tarea => tarea.description === description);
    if (borrado >= 0) {
        if (borrado == listadoPorHacer.length) {
            listadoPorHacer.pop();
        } else {
            listadoPorHacer.splice(borrado, 1);
        }
        saveDB();
        return true;
    } else {
        console.log(`No existe la tarea "${description}"`);
        return false;
    }

    ES LO MISMO QUE ESTO
    */
    let borrado = listadoPorHacer.filter(tarea => tarea.description !== description);

    if (listadoPorHacer.length === nuevoListado.length) {
        console.log(`No existe la tarea "${description}"`);
        return false;
    } else {
        listadoPorHacer = borrado;
        saveDB();
        return true;
    }
}

module.exports = {
    crear,
    getList,
    check,
    remove
}