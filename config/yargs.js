const description = {
    alias: 'd',
    demand: true,
    desc: 'Descripción de la tarea'
};

const completed = {
    alias: 'c',
    desc: 'Marca la tarea como completada o pendiente'
};

const argv = require('yargs')
    .command('new', 'Crear una tarea', {
        description,
    })
    .help()
    .command('list', 'Lista tus tareas', {
        completed,
    })
    .help()
    .command('check', 'Actualizar estado de una tarea', {
        completed,
        description,
    })
    .help()
    .command('remove', 'Elimina una tarea', {
        description,
    })
    .help()
    .argv;

module.exports = {
    argv
}